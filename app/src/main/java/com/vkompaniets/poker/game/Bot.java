package com.vkompaniets.poker.game;

import com.vkompaniets.poker.game.UserInteractionHandler.UserAction;

import java.util.Random;

import static com.vkompaniets.poker.game.UserInteractionHandler.UserAction.CALL;
import static com.vkompaniets.poker.game.UserInteractionHandler.UserAction.CHECK;
import static com.vkompaniets.poker.game.UserInteractionHandler.UserAction.FOLD;
import static com.vkompaniets.poker.game.UserInteractionHandler.UserAction.RAISE;

/**
 * Created by vkompaniets on 01.08.2017.
 */

public class Bot extends Player {

    //call, raise, fold, check

    private UserInteractionHandler handler;
    private Random random = new Random();

    public Bot(int chips, UserInteractionHandler handler) {
        super(chips);
        this.handler = handler;
    }

    @Override
    public boolean isHuman() {
        return false;
    }

    public void simulateUserActivity(int maxBet){
        int difference = maxBet - getBet();

        UserAction action;
        if (difference > 0){
            //raise, call, fold
            action = generateRiseCallFold();
        }else{
            //raise, check
            action = generateRiseCheck();
        }

        switch (action){
            case CALL:
                handler.onPlayerCall(difference);
                break;
            case RAISE:
                handler.onPlayerRaise(difference + 100);
                break;
            case FOLD:
                handler.onPlayerFold();
                break;
            case CHECK:
                handler.onPlayerCheck();
                break;
        }
    }

    private UserAction generateRiseCallFold(){
        double value = random.nextDouble();
        if (value < 0.20)
            return RAISE;
        else if (value < 90)
            return CALL;
        else
            return FOLD;
    }

    private UserAction generateRiseCheck(){
        double value = random.nextDouble();
        if (value < 0.20)
            return RAISE;
        else
            return CHECK;
    }
}
