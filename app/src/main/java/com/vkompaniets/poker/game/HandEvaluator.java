package com.vkompaniets.poker.game;

import com.vkompaniets.poker.deck.Card;
import com.vkompaniets.poker.deck.Rank;
import com.vkompaniets.poker.deck.Suit;

import org.apache.commons.lang3.ArrayUtils;

import static com.vkompaniets.poker.deck.Card.SEVEN_OF_SPADES;
import static com.vkompaniets.poker.deck.Card.SIX_OF_SPADES;
import static com.vkompaniets.poker.deck.Card.TEN_OF_HEARTS;
import static com.vkompaniets.poker.deck.Card.TEN_OF_SPADES;
import static com.vkompaniets.poker.deck.Card.THE_ACE_OF_SPADES;
import static com.vkompaniets.poker.deck.Card.TWO_OF_CLUBS;
import static com.vkompaniets.poker.deck.Card.TWO_OF_DIAMONDS;
import static com.vkompaniets.poker.deck.Rank.ACE;
import static com.vkompaniets.poker.deck.Rank.FIVE;
import static com.vkompaniets.poker.deck.Rank.FOUR;
import static com.vkompaniets.poker.deck.Rank.TEN;
import static com.vkompaniets.poker.deck.Rank.THREE;
import static com.vkompaniets.poker.deck.Rank.TWO;

/**
 * Created by vkompaniets on 01.08.2017.
 */

public class HandEvaluator {

    private static final int DEPTH = 3;

    private enum Combination{
        HIGH_CARD,
        PAIR,
        TWO_PAIRS,
        THREE_OF_A_KIND,
        STRAIGHT,
        FLUSH,
        FULL_HOUSE,
        FOUR_OF_A_KIND,
        STRAIGHT_FLUSH,
        ROYAL_FLUSH;

        public static Combination valueOf(int value){
            for (Combination combination : Combination.values()){
                if (value == combination.ordinal())
                    return combination;
            }

            return null;
        }
    }

    public int whoWins(Card[] first, Card[] second, Card[] common){
        int[] firstValue = evaluate(ArrayUtils.addAll(first, common));
        int[] secondValue = evaluate(ArrayUtils.addAll(second, common));

        int result;
        if (firstValue[0] != secondValue[0])
            result = firstValue[0] - secondValue[0];
        else if (firstValue[1] != secondValue[1])
            result = firstValue[1] - secondValue[1];
        else
            result = firstValue[2] - secondValue[2];

        if (result > 0)
            return 0;
        else if (result < 0)
            return 1;
        else return -1;
    }

    private int[] evaluate(Card[] cards){
        int[] evaluation = new int[DEPTH];
        int[] ranks = new int[Rank.values().length];
        int[] suits = new int[Suit.values().length];

        int largeGroupSize = -1;
        int largeGroupRank = -1;

        int smallGroupSize = -1;
        int smallGroupRank = -1;

        int straightRank = -1;
        int flushSuit = -1;

        for (Card card : cards){
            ranks[card.rank.ordinal()]++;
            suits[card.suit.ordinal()]++;
        }

        for (int i = 0; i < Rank.values().length; i++){
            int count = ranks[i];
            if (count >= largeGroupSize){
                smallGroupSize = largeGroupSize;
                smallGroupRank = largeGroupRank;

                largeGroupSize = count;
                largeGroupRank = i;
            }else if (count >= smallGroupSize){
                smallGroupSize = count;
                smallGroupRank = i;
            }
        }

        if (ranks[TWO.ordinal()] > 0 && ranks[THREE.ordinal()] > 0 && ranks[FOUR.ordinal()] > 0 && ranks[FIVE.ordinal()] > 0 && ranks[ACE.ordinal()] > 0){
            straightRank = FIVE.ordinal();
        }

        for (int i = TWO.ordinal(); i <= TEN.ordinal(); i++){
            if (ranks[i] > 0 && ranks[i+1] > 0 && ranks[i+2] > 0 && ranks[i+3] > 0 && ranks[i+4] > 0){
                straightRank = i + 4;
            }
        }

        for (int i = 0; i < suits.length; i++){
            if (suits[i] >= 5){
                flushSuit = i;
                break;
            }
        }

        //high card
        if (largeGroupSize == 1){
            evaluation[0] = 0;
            evaluation[1] = largeGroupRank;
            evaluation[2] = 0;
        }

        //pair
        if (largeGroupSize == 2 && smallGroupSize == 1){
            evaluation[0] = 1;
            evaluation[1] = largeGroupRank;
            evaluation[2] = 0;
        }

        //two pairs
        if (largeGroupSize == 2 && smallGroupSize == 2){
            evaluation[0] = 2;
            evaluation[1] = largeGroupRank;
            evaluation[2] = smallGroupRank;
        }

        //three
        if (largeGroupSize == 3 && smallGroupSize == 1){
            evaluation[0] = 3;
            evaluation[1] = largeGroupRank;
            evaluation[2] = 0;
        }

        //straight
        if (straightRank > 0){
            evaluation[0] = 4;
            evaluation[1] = straightRank;
            evaluation[2] = 0;
        }

        //flush
        if (flushSuit > 0){
            int flushRank = -1;
            for (Card card : cards){
                if (card.suit.ordinal() == flushSuit && card.rank.ordinal() > flushRank){
                    flushRank = card.rank.ordinal();
                }
            }
            evaluation[0] = 5;
            evaluation[1] = flushRank;
            evaluation[2] = 0;
        }

        //full house
        if (largeGroupSize == 3 && smallGroupSize == 2){
            evaluation[0] = 6;
            evaluation[1] = largeGroupRank;
            evaluation[2] = smallGroupRank;
        }

        //four
        if (largeGroupSize == 4){
            int kickerRank = -1;
            for (Card card : cards){
                if (card.rank.ordinal() != largeGroupRank && card.rank.ordinal() > kickerRank){
                    kickerRank = card.rank.ordinal();
                }
            }
            evaluation[0] = 7;
            evaluation[1] = largeGroupRank;
            evaluation[2] = kickerRank;
        }

        //TODO: straight flush
        //TODO: royal flush

        return evaluation;
    }

    public static void main (String... args){
        Card[] flop = new Card[]{THE_ACE_OF_SPADES, TWO_OF_CLUBS, SEVEN_OF_SPADES, SIX_OF_SPADES, TWO_OF_DIAMONDS};
        Card[] hand = new Card[]{TEN_OF_HEARTS, TEN_OF_SPADES};

        Card[] both = ArrayUtils.addAll(flop, hand);
        HandEvaluator cm = new HandEvaluator();
        int[] values = cm.evaluate(both);
        System.out.print(Combination.valueOf(values[0]));
    }

}
