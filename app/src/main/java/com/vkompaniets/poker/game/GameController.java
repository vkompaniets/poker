package com.vkompaniets.poker.game;

import com.vkompaniets.poker.PokerDesk;
import com.vkompaniets.poker.deck.Card;
import com.vkompaniets.poker.deck.Deck;

import static com.vkompaniets.poker.game.GameController.Round.FLOP;
import static com.vkompaniets.poker.game.GameController.Round.PRE_FLOP;
import static com.vkompaniets.poker.game.GameController.Round.RIVER;
import static com.vkompaniets.poker.game.GameController.Round.SHOWDOWN;
import static com.vkompaniets.poker.game.GameController.Round.TURN;

/**
 * Created by vkompaniets on 01.08.2017.
 */

public class GameController implements UserInteractionHandler{

    public enum Round {
        PRE_FLOP, FLOP, TURN, RIVER, SHOWDOWN
    }

    private static final int STARTING_CHIPS = 10000;
    private static final int SMALL_BLIND = 5;
    private static final int BLIND = 10;

    private PokerDesk desk;

    private HandEvaluator handEvaluator;
    private Deck deck;
    private int bank;
    private int maxBet;
    private Player[] players;
    private int playedThisRound;


    private int dealer;
    private int currentPlayer;
    private Round currentRound;

    private Card[] table;

    public int getMaxBet(){
        return maxBet;
    }

    public Player getPlayer(){
        return players[0];
    }

    public GameController(PokerDesk desk) {
        this.desk = desk;
        this.deck = new Deck();
        this.handEvaluator = new HandEvaluator();
    }

    public void newTournament(){
        desk.showMessage("New tournament started");

        dealer = -1;
        players = new Player[2];
        players[0] = new Human(STARTING_CHIPS);
        players[1] = new Bot(STARTING_CHIPS, this);

        nextGame();
    }

    public void nextGame(){
        desk.showMessage("New game started");

        bank = 0;
        deck.shuffle();
        table = new Card[5];
        nextDealer();
        updateUI();

        proceedToPreflop();
    }

    private void proceedToPreflop(){
        setRound(PRE_FLOP);

        try {
            bank += nextPlayer().putBlind(SMALL_BLIND);
            bank += nextPlayer().putBlind(BLIND);
            maxBet = BLIND;
            playedThisRound = 1;
        } catch (NotEnoughChipsException e) {
            endTournament();
            return;
        }

        drawCardsToPlayers();
        desk.showMessage("Blinds are done");
        desk.showMessage("Cards drawn to players");
        updateUI();

        doBets();
    }

    private void proceedToFlop(){
        setRound(FLOP);
        table[0] = deck.drawCard();
        table[1] = deck.drawCard();
        table[2] = deck.drawCard();
        doBets();
    }

    private void proceedToTurn(){
        setRound(TURN);
        table[3] = deck.drawCard();

        doBets();
    }

    private void proceedToRiver(){
        setRound(RIVER);
        table[4] = deck.drawCard();

        doBets();
    }

    private void proceedToShowdown(){
        setRound(SHOWDOWN);
        int whoWins = handEvaluator.whoWins(players[0].getCards(), players[1].getCards(), table);
        if (whoWins == -1){
            players[0].winGame(bank / 2);
            players[1].winGame(bank / 2);
            desk.showMessage("It's a draw! Bank is divided");
        }else{
            players[whoWins].winGame(bank);
            desk.showMessage(String.format("Player %d won the game", whoWins));
        }
        updateUI();

    }

    private void setRound(Round round){
        desk.showMessage(round + " started");
        currentRound = round;
        currentPlayer = dealer;
        playedThisRound = 0;
        for(Player player : players){
            player.setBet(0);
        }
        maxBet = 0;

        updateUI();
    }

    private void drawCardsToPlayers(){
        int playerToDraw = dealer;
        for (int i = 0; i < 2; i++){
            for (int j = 0; j < players.length; j++){
                playerToDraw++;
                if (playerToDraw == players.length)
                    playerToDraw = 0;
                players[playerToDraw].takeCard(deck.drawCard());
            }
        }
    }

    private void doBets(){
        if (playedThisRound == players.length){
            nextRound();
        }else{
            Player player = nextPlayer();
            if (player.isHuman()){
                updateUI();
            }else{
                ((Bot) player).simulateUserActivity(maxBet);
                updateUI();
            }
        }
    }

    private Player nextPlayer(){
        currentPlayer++;
        if (currentPlayer == players.length)
            currentPlayer = 0;

        return players[currentPlayer];
    }

    private Player nextDealer(){
        dealer++;
        if (dealer == players.length) {
            dealer = 0;
        }
        desk.showMessage(String.format("Player %d is a dealer", currentPlayer));
        return players[dealer];
    }

    private void nextRound(){
        switch (currentRound){
            case PRE_FLOP:
                proceedToFlop();
                break;
            case FLOP:
                proceedToTurn();
                break;
            case TURN:
                proceedToRiver();
                break;
            case RIVER:
                proceedToShowdown();
                break;
            case SHOWDOWN:
                break;
        }
    }

    private void endTournament(){
        desk.showMessage("Tournament finished");
    }

    private void updateUI(){
        desk.renderTable(table, bank);
        desk.renderPlayer(players[0], maxBet, dealer == 0, currentRound == SHOWDOWN);
        desk.renderPlayer(players[1], maxBet, dealer == 1, currentRound == SHOWDOWN);
    }

    @Override
    public void onPlayerCall(int amount) {
        playedThisRound++;
        desk.showMessage(String.format("Player %d CALLS for %d", currentPlayer, amount));
        bank += players[currentPlayer].doBet(amount);
        doBets();
    }

    @Override
    public void onPlayerRaise(int amount) {
        playedThisRound = 1;
        desk.showMessage(String.format("Player %d RAISES for %d", currentPlayer, amount));
        bank += players[currentPlayer].doBet(amount);
        maxBet = players[currentPlayer].getBet();
        doBets();
    }

    @Override
    public void onPlayerFold() {
        playedThisRound++;
        desk.showMessage(String.format("Player %d FOLDS", currentPlayer));
        nextPlayer().winGame(bank);
        desk.showMessage(String.format("Player %d wins the game!", currentPlayer));
    }

    @Override
    public void onPlayerCheck() {
        playedThisRound++;
        desk.showMessage(String.format("Player %d CHECKS", currentPlayer));
        doBets();
    }
}
