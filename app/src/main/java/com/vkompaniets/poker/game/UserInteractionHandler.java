package com.vkompaniets.poker.game;

/**
 * Created by vkompaniets on 02.08.2017.
 */

public interface UserInteractionHandler {

    enum UserAction {
        CALL, RAISE, FOLD, CHECK
    }

    void onPlayerCall(int amount);
    void onPlayerRaise(int amount);
    void onPlayerFold();
    void onPlayerCheck();
}
