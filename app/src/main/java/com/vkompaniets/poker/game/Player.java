package com.vkompaniets.poker.game;

import com.vkompaniets.poker.deck.Card;

/**
 * Created by vkompaniets on 02.08.2017.
 */

public abstract class Player {

    private int availableChips;
    private int bet;
    private Card[] cards;

    public Player(int availableChips) {
        this.availableChips = availableChips;
    }

    public int putBlind(int amount) throws NotEnoughChipsException {
        if (availableChips < amount)
            throw new NotEnoughChipsException();

        availableChips -= amount;
        bet += amount;
        return amount;
    }

    public abstract boolean isHuman();

    public int doBet(int amount) {
        availableChips -= amount;
        bet += amount;
        return amount;
    }

    public void takeCard(Card card) {
        if (cards == null){
            cards = new Card[2];
            cards[0] = card;
        }else{
            cards[1] = card;
        }
    }

    public void winGame(int bank) {
        availableChips += bank;
    }



    public int getAvailableChips() {
        return availableChips;
    }

    public int getBet() {
        return bet;
    }

    public void setBet(int bet) {
        this.bet = bet;
    }

    public Card[] getCards() {
        return cards;
    }
}
