package com.vkompaniets.poker.game;

/**
 * Created by vkompaniets on 01.08.2017.
 */

public class Human extends Player {

    public Human(int chips) {
        super(chips);
    }

    @Override
    public boolean isHuman() {
        return true;
    }
}
