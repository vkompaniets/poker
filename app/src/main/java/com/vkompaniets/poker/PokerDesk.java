package com.vkompaniets.poker;

import com.vkompaniets.poker.deck.Card;
import com.vkompaniets.poker.game.Player;

/**
 * Created by vkompaniets on 01.08.2017.
 */

public interface PokerDesk {
    void showMessage(String msg);
    void renderTable(Card[] cards, int bank);
    void renderPlayer(Player player, int maxBet, boolean isDealer, boolean isShowdown);
}
