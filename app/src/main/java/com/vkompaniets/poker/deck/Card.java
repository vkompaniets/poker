package com.vkompaniets.poker.deck;

import static com.vkompaniets.poker.deck.Rank.ACE;
import static com.vkompaniets.poker.deck.Rank.EIGHT;
import static com.vkompaniets.poker.deck.Rank.FIVE;
import static com.vkompaniets.poker.deck.Rank.FOUR;
import static com.vkompaniets.poker.deck.Rank.JACK;
import static com.vkompaniets.poker.deck.Rank.KING;
import static com.vkompaniets.poker.deck.Rank.NINE;
import static com.vkompaniets.poker.deck.Rank.QUEEN;
import static com.vkompaniets.poker.deck.Rank.SEVEN;
import static com.vkompaniets.poker.deck.Rank.SIX;
import static com.vkompaniets.poker.deck.Rank.TEN;
import static com.vkompaniets.poker.deck.Rank.THREE;
import static com.vkompaniets.poker.deck.Rank.TWO;
import static com.vkompaniets.poker.deck.Suit.CLUBS;
import static com.vkompaniets.poker.deck.Suit.DIAMONDS;
import static com.vkompaniets.poker.deck.Suit.HEARTS;
import static com.vkompaniets.poker.deck.Suit.SPADES;

/**
 * Created by vkompaniets on 01.08.2017.
 */

public enum Card {

    TWO_OF_CLUBS(TWO, CLUBS),
    TWO_OF_DIAMONDS(TWO, DIAMONDS),
    TWO_OF_HEARTS(TWO, HEARTS),
    TWO_OF_SPADES(TWO, SPADES),

    THREE_OF_CLUBS(THREE, CLUBS),
    THREE_OF_DIAMONDS(THREE, DIAMONDS),
    THREE_OF_HEARTS(THREE, HEARTS),
    THREE_OF_SPADES(THREE, SPADES),

    FOUR_OF_CLUBS(FOUR, CLUBS),
    FOUR_OF_DIAMONDS(FOUR, DIAMONDS),
    FOUR_OF_HEARTS(FOUR, HEARTS),
    FOUR_OF_SPADES(FOUR, SPADES),

    FIVE_OF_CLUBS(FIVE, CLUBS),
    FIVE_OF_DIAMONDS(FIVE, DIAMONDS),
    FIVE_OF_HEARTS(FIVE, HEARTS),
    FIVE_OF_SPADES(FIVE, SPADES),

    SIX_OF_CLUBS(SIX, CLUBS),
    SIX_OF_DIAMONDS(SIX, DIAMONDS),
    SIX_OF_HEARTS(SIX, HEARTS),
    SIX_OF_SPADES(SIX, SPADES),

    SEVEN_OF_CLUBS(SEVEN, CLUBS),
    SEVEN_OF_DIAMONDS(SEVEN, DIAMONDS),
    SEVEN_OF_HEARTS(SEVEN, HEARTS),
    SEVEN_OF_SPADES(SEVEN, SPADES),

    EIGHT_OF_CLUBS(EIGHT, CLUBS),
    EIGHT_OF_DIAMONDS(EIGHT, DIAMONDS),
    EIGHT_OF_HEARTS(EIGHT, HEARTS),
    EIGHT_OF_SPADES(EIGHT, SPADES),

    NINE_OF_CLUBS(NINE, CLUBS),
    NINE_OF_DIAMONDS(NINE, DIAMONDS),
    NINE_OF_HEARTS(NINE, HEARTS),
    NINE_OF_SPADES(NINE, SPADES),

    TEN_OF_CLUBS(TEN, CLUBS),
    TEN_OF_DIAMONDS(TEN, DIAMONDS),
    TEN_OF_HEARTS(TEN, HEARTS),
    TEN_OF_SPADES(TEN, SPADES),

    JACK_OF_CLUBS(JACK, CLUBS),
    JACK_OF_DIAMONDS(JACK, DIAMONDS),
    JACK_OF_HEARTS(JACK, HEARTS),
    JACK_OF_SPADES(JACK, SPADES),

    QUEEN_OF_CLUBS(QUEEN, CLUBS),
    QUEEN_OF_DIAMONDS(QUEEN, DIAMONDS),
    QUEEN_OF_HEARTS(QUEEN, HEARTS),
    QUEEN_OF_SPADES(QUEEN, SPADES),

    KING_OF_CLUBS(KING, CLUBS),
    KING_OF_DIAMONDS(KING, DIAMONDS),
    KING_OF_HEARTS(KING, HEARTS),
    KING_OF_SPADES(KING, SPADES),

    ACE_OF_CLUBS(ACE, CLUBS),
    ACE_OF_DIAMONDS(ACE, DIAMONDS),
    ACE_OF_HEARTS(ACE, HEARTS),
    THE_ACE_OF_SPADES(ACE, SPADES);

    Card(Rank rank, Suit suit) {
        this.rank = rank;
        this.suit = suit;
    }

    public final Rank rank;
    public final Suit suit;
}

