package com.vkompaniets.poker.deck;

import java.util.Arrays;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by vkompaniets on 01.08.2017.
 */

public class Deck {

    private List<Card> deck;

    public void shuffle(){
        deck = new LinkedList<>(Arrays.asList(Card.values()));
        Collections.shuffle(deck);
    }

    public Card drawCard(){
        return deck.remove(0);
    }
}
