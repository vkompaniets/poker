package com.vkompaniets.poker.deck;

/**
 * Created by vkompaniets on 01.08.2017.
 */

public enum Rank {
    TWO, THREE, FOUR, FIVE, SIX, SEVEN, EIGHT, NINE, TEN, JACK, QUEEN, KING, ACE
}
