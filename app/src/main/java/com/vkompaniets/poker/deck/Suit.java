package com.vkompaniets.poker.deck;

/**
 * Created by vkompaniets on 01.08.2017.
 */

public enum Suit {
    CLUBS, DIAMONDS, HEARTS, SPADES
}
