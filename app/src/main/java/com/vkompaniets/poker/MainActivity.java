package com.vkompaniets.poker;

import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.vkompaniets.poker.deck.Card;
import com.vkompaniets.poker.game.GameController;
import com.vkompaniets.poker.game.Player;

public class MainActivity extends AppCompatActivity implements PokerDesk, OnClickListener {

    private static final String TAG = MainActivity.class.getSimpleName();

    private GameController gameController;

    private TextView bankView;
    private ImageView[] tableCardViews;
    private TextView actionsView;

    private TextView p0NameView;
    private ImageView[] p0CardViews;
    private TextView p0ChipsView;
    private TextView p0BetView;

    private TextView p1NameView;
    private ImageView[] p1CardViews;
    private TextView p1ChipsView;
    private TextView p1BetView;

    private Button checkBtn;
    private Button callBtn;
    private Button raiseBtn;
    private Button foldBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        checkBtn = (Button) findViewById(R.id.check);
        callBtn = (Button) findViewById(R.id.call);
        raiseBtn = (Button) findViewById(R.id.raise);
        foldBtn = (Button) findViewById(R.id.fold);

        checkBtn.setOnClickListener(this);
        callBtn.setOnClickListener(this);
        raiseBtn.setOnClickListener(this);
        foldBtn.setOnClickListener(this);

        tableCardViews = new ImageView[5];
        tableCardViews[0] = (ImageView) findViewById(R.id.card0);
        tableCardViews[1] = (ImageView) findViewById(R.id.card1);
        tableCardViews[2] = (ImageView) findViewById(R.id.card2);
        tableCardViews[3] = (ImageView) findViewById(R.id.card3);
        tableCardViews[4] = (ImageView) findViewById(R.id.card4);

        bankView = (TextView) findViewById(R.id.bank);
        actionsView = (TextView) findViewById(R.id.actions);

        p0CardViews = new ImageView[2];
        p0CardViews[0] = (ImageView) findViewById(R.id.p0_card0);
        p0CardViews[1] = (ImageView) findViewById(R.id.p0_card1);
        p0ChipsView = (TextView) findViewById(R.id.p0_chips);
        p0BetView = (TextView) findViewById(R.id.p0_bet);
        p0NameView = (TextView) findViewById(R.id.p0_name);

        p1CardViews = new ImageView[2];
        p1CardViews[0] = (ImageView) findViewById(R.id.p1_card0);
        p1CardViews[1] = (ImageView) findViewById(R.id.p1_card1);
        p1ChipsView = (TextView) findViewById(R.id.p1_chips);
        p1BetView = (TextView) findViewById(R.id.p1_bet);
        p1NameView = (TextView) findViewById(R.id.p1_name);

        playTournament();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.new_game:
                newGame();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void playTournament(){
        gameController = new GameController(this);
        gameController.newTournament();
    }

    private void newGame(){
        actionsView.setText(null);
        gameController.nextGame();
    }

    @Override
    public void showMessage(String msg) {
        actionsView.setText(msg + "\n" + actionsView.getText());
        Log.d(TAG, msg);
    }

    @Override
    public void renderTable(Card[] cards, int bank){
        for (int i = 0; i < cards.length; i++){
            if (cards[i] == null){
                tableCardViews[i].setVisibility(View.INVISIBLE);
            }else{
                tableCardViews[i].setVisibility(View.VISIBLE);
                tableCardViews[i].setImageResource(CardResources.getFor(cards[i]));
            }
        }

        bankView.setText(money(bank));
    }

    @Override
    public void renderPlayer(Player player, int maxBet, boolean isDealer, boolean isShowdown){
        TextView name;
        TextView chips;
        TextView bet;
        ImageView[] cards;
        if (player.isHuman()){
            name = this.p0NameView;
            chips = this.p0ChipsView;
            bet = this.p0BetView;
            cards = this.p0CardViews;
        }else{
            name = this.p1NameView;
            chips = this.p1ChipsView;
            bet = this.p1BetView;
            cards = this.p1CardViews;
        }

        name.setTypeface(null, isDealer ? Typeface.BOLD : Typeface.NORMAL);
        chips.setText(money(player.getAvailableChips()));
        bet.setText(money(player.getBet()));

        if (player.getCards() == null){
            for (ImageView imageView : cards){
                imageView.setVisibility(View.INVISIBLE);
            }
        }else{
            if (player.isHuman() || isShowdown){
                for (int i = 0; i < player.getCards().length; i++){
                    if (player.getCards()[i] == null){
                        cards[i].setVisibility(View.INVISIBLE);
                    }else{
                        cards[i].setVisibility(View.VISIBLE);
                        cards[i].setImageResource(CardResources.getFor(player.getCards()[i]));
                    }
                }
            }else{
                for (ImageView imageView : cards){
                    imageView.setVisibility(View.VISIBLE);
                    imageView.setImageResource(R.drawable.card_back);
                }
            }
        }

        if (player.isHuman()){
            int difference = maxBet - player.getBet();
            if (difference > 0){
                callBtn.setEnabled(true);
                foldBtn.setEnabled(true);
                checkBtn.setEnabled(false);
            }else{
                callBtn.setEnabled(false);
                foldBtn.setEnabled(false);
                checkBtn.setEnabled(true);
            }

            callBtn.setText("CALL " + difference);
        }
    }

    @Override
    public void onClick(View v) {
        int difference = gameController.getMaxBet() - gameController.getPlayer().getBet();
        switch (v.getId()){
            case R.id.check:
                gameController.onPlayerCheck();
                break;
            case R.id.call:
                gameController.onPlayerCall(difference);
                break;
            case R.id.raise:
                gameController.onPlayerRaise(difference + 100);
                break;
            case R.id.fold:
                gameController.onPlayerFold();
                break;
        }
    }

    private static String money(int chips){
        return "$" + chips;
    }
}
