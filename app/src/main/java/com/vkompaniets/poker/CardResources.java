package com.vkompaniets.poker;

import com.vkompaniets.poker.deck.Card;

import java.util.HashMap;

import static com.vkompaniets.poker.deck.Card.ACE_OF_CLUBS;
import static com.vkompaniets.poker.deck.Card.ACE_OF_DIAMONDS;
import static com.vkompaniets.poker.deck.Card.ACE_OF_HEARTS;
import static com.vkompaniets.poker.deck.Card.EIGHT_OF_CLUBS;
import static com.vkompaniets.poker.deck.Card.EIGHT_OF_DIAMONDS;
import static com.vkompaniets.poker.deck.Card.EIGHT_OF_HEARTS;
import static com.vkompaniets.poker.deck.Card.EIGHT_OF_SPADES;
import static com.vkompaniets.poker.deck.Card.FIVE_OF_CLUBS;
import static com.vkompaniets.poker.deck.Card.FIVE_OF_DIAMONDS;
import static com.vkompaniets.poker.deck.Card.FIVE_OF_HEARTS;
import static com.vkompaniets.poker.deck.Card.FIVE_OF_SPADES;
import static com.vkompaniets.poker.deck.Card.FOUR_OF_CLUBS;
import static com.vkompaniets.poker.deck.Card.FOUR_OF_DIAMONDS;
import static com.vkompaniets.poker.deck.Card.FOUR_OF_HEARTS;
import static com.vkompaniets.poker.deck.Card.FOUR_OF_SPADES;
import static com.vkompaniets.poker.deck.Card.JACK_OF_CLUBS;
import static com.vkompaniets.poker.deck.Card.JACK_OF_DIAMONDS;
import static com.vkompaniets.poker.deck.Card.JACK_OF_HEARTS;
import static com.vkompaniets.poker.deck.Card.JACK_OF_SPADES;
import static com.vkompaniets.poker.deck.Card.KING_OF_CLUBS;
import static com.vkompaniets.poker.deck.Card.KING_OF_DIAMONDS;
import static com.vkompaniets.poker.deck.Card.KING_OF_HEARTS;
import static com.vkompaniets.poker.deck.Card.KING_OF_SPADES;
import static com.vkompaniets.poker.deck.Card.NINE_OF_CLUBS;
import static com.vkompaniets.poker.deck.Card.NINE_OF_DIAMONDS;
import static com.vkompaniets.poker.deck.Card.NINE_OF_HEARTS;
import static com.vkompaniets.poker.deck.Card.NINE_OF_SPADES;
import static com.vkompaniets.poker.deck.Card.QUEEN_OF_CLUBS;
import static com.vkompaniets.poker.deck.Card.QUEEN_OF_DIAMONDS;
import static com.vkompaniets.poker.deck.Card.QUEEN_OF_HEARTS;
import static com.vkompaniets.poker.deck.Card.QUEEN_OF_SPADES;
import static com.vkompaniets.poker.deck.Card.SEVEN_OF_CLUBS;
import static com.vkompaniets.poker.deck.Card.SEVEN_OF_DIAMONDS;
import static com.vkompaniets.poker.deck.Card.SEVEN_OF_HEARTS;
import static com.vkompaniets.poker.deck.Card.SEVEN_OF_SPADES;
import static com.vkompaniets.poker.deck.Card.SIX_OF_CLUBS;
import static com.vkompaniets.poker.deck.Card.SIX_OF_DIAMONDS;
import static com.vkompaniets.poker.deck.Card.SIX_OF_HEARTS;
import static com.vkompaniets.poker.deck.Card.SIX_OF_SPADES;
import static com.vkompaniets.poker.deck.Card.TEN_OF_CLUBS;
import static com.vkompaniets.poker.deck.Card.TEN_OF_DIAMONDS;
import static com.vkompaniets.poker.deck.Card.TEN_OF_HEARTS;
import static com.vkompaniets.poker.deck.Card.TEN_OF_SPADES;
import static com.vkompaniets.poker.deck.Card.THE_ACE_OF_SPADES;
import static com.vkompaniets.poker.deck.Card.THREE_OF_CLUBS;
import static com.vkompaniets.poker.deck.Card.THREE_OF_DIAMONDS;
import static com.vkompaniets.poker.deck.Card.THREE_OF_HEARTS;
import static com.vkompaniets.poker.deck.Card.THREE_OF_SPADES;
import static com.vkompaniets.poker.deck.Card.TWO_OF_CLUBS;
import static com.vkompaniets.poker.deck.Card.TWO_OF_DIAMONDS;
import static com.vkompaniets.poker.deck.Card.TWO_OF_HEARTS;
import static com.vkompaniets.poker.deck.Card.TWO_OF_SPADES;

/**
 * Created by Vladimir on 03.08.2017.
 */

public class CardResources {

    private static HashMap<Card, Integer> map = new HashMap<>();
    static {
        map.put(TWO_OF_CLUBS, R.drawable.two_of_clubs);
        map.put(TWO_OF_DIAMONDS, R.drawable.two_of_diamonds);
        map.put(TWO_OF_HEARTS, R.drawable.two_of_hearts);
        map.put(TWO_OF_SPADES, R.drawable.two_of_spades);

        map.put(THREE_OF_CLUBS, R.drawable.three_of_clubs);
        map.put(THREE_OF_DIAMONDS, R.drawable.three_of_diamonds);
        map.put(THREE_OF_HEARTS, R.drawable.three_of_hearts);
        map.put(THREE_OF_SPADES, R.drawable.three_of_spades);

        map.put(FOUR_OF_CLUBS, R.drawable.four_of_clubs);
        map.put(FOUR_OF_DIAMONDS, R.drawable.four_of_diamonds);
        map.put(FOUR_OF_HEARTS, R.drawable.four_of_hearts);
        map.put(FOUR_OF_SPADES, R.drawable.four_of_spades);

        map.put(FIVE_OF_CLUBS, R.drawable.five_of_clubs);
        map.put(FIVE_OF_DIAMONDS, R.drawable.five_of_diamonds);
        map.put(FIVE_OF_HEARTS, R.drawable.five_of_hearts);
        map.put(FIVE_OF_SPADES, R.drawable.five_of_spades);

        map.put(SIX_OF_CLUBS, R.drawable.six_of_clubs);
        map.put(SIX_OF_DIAMONDS, R.drawable.six_of_diamonds);
        map.put(SIX_OF_HEARTS, R.drawable.six_of_hearts);
        map.put(SIX_OF_SPADES, R.drawable.six_of_spades);

        map.put(SEVEN_OF_CLUBS, R.drawable.seven_of_clubs);
        map.put(SEVEN_OF_DIAMONDS, R.drawable.seven_of_diamonds);
        map.put(SEVEN_OF_HEARTS, R.drawable.seven_of_hearts);
        map.put(SEVEN_OF_SPADES, R.drawable.seven_of_spades);

        map.put(EIGHT_OF_CLUBS, R.drawable.eight_of_clubs);
        map.put(EIGHT_OF_DIAMONDS, R.drawable.eight_of_diamonds);
        map.put(EIGHT_OF_HEARTS, R.drawable.eight_of_hearts);
        map.put(EIGHT_OF_SPADES, R.drawable.eight_of_spades);

        map.put(NINE_OF_CLUBS, R.drawable.nine_of_clubs);
        map.put(NINE_OF_DIAMONDS, R.drawable.nine_of_diamonds);
        map.put(NINE_OF_HEARTS, R.drawable.nine_of_hearts);
        map.put(NINE_OF_SPADES, R.drawable.nine_of_spades);

        map.put(TEN_OF_CLUBS, R.drawable.ten_of_clubs);
        map.put(TEN_OF_DIAMONDS, R.drawable.ten_of_diamonds);
        map.put(TEN_OF_HEARTS, R.drawable.ten_of_hearts);
        map.put(TEN_OF_SPADES, R.drawable.ten_of_spades);

        map.put(JACK_OF_CLUBS, R.drawable.jack_of_clubs2);
        map.put(JACK_OF_DIAMONDS, R.drawable.jack_of_diamonds2);
        map.put(JACK_OF_HEARTS, R.drawable.jack_of_hearts2);
        map.put(JACK_OF_SPADES, R.drawable.jack_of_spades2);

        map.put(QUEEN_OF_CLUBS, R.drawable.queen_of_clubs2);
        map.put(QUEEN_OF_DIAMONDS, R.drawable.queen_of_diamonds2);
        map.put(QUEEN_OF_HEARTS, R.drawable.queen_of_hearts2);
        map.put(QUEEN_OF_SPADES, R.drawable.queen_of_spades2);

        map.put(KING_OF_CLUBS, R.drawable.king_of_clubs2);
        map.put(KING_OF_DIAMONDS, R.drawable.king_of_diamonds2);
        map.put(KING_OF_HEARTS, R.drawable.king_of_hearts2);
        map.put(KING_OF_SPADES, R.drawable.king_of_spades2);

        map.put(ACE_OF_CLUBS, R.drawable.ace_of_clubs);
        map.put(ACE_OF_DIAMONDS, R.drawable.ace_of_diamonds);
        map.put(ACE_OF_HEARTS, R.drawable.ace_of_hearts);
        map.put(THE_ACE_OF_SPADES, R.drawable.ace_of_spades);
    }

    public static int getFor(Card card){
        return map.get(card);
    }

    private CardResources() {}
}
